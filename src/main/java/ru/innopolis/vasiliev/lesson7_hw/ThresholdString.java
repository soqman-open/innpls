package ru.innopolis.vasiliev.lesson7_hw;

public class ThresholdString {
    private String s;
    private boolean isFull;
    private int threshold;

    public String getString() {
        return s;
    }

    public void addString(String s) {
        this.s += s;
        check();
    }

    public boolean isFull() {
        return isFull;
    }

    public long getThreshold() {
        return threshold;
    }

    public void setThreshold(int threshold) {
        if(threshold<0)threshold=0;
        this.threshold = threshold;
        check();
    }

    public ThresholdString(int threshold) {
        this.threshold = threshold;
        s="";
    }

    public void clean(){
        s="";
        check();
    }

    private void check(){
        if(s.length()>=threshold){
            isFull=true;
        }
        else{
            isFull=false;
        }
    }


}
