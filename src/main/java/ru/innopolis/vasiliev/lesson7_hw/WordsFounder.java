package ru.innopolis.vasiliev.lesson7_hw;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class WordsFounder {
    private final String[] words;
    private final static Logger logger=LogManager.getLogger(ScanningRunnable.class);

    public WordsFounder(String[] words) {
        this.words = words;
    }

    public boolean search(String sentence) {
        for (String word : words) {
            if (isWordInSentence(sentence, word)) {
                logger.info("word \"" +word+"\" found!");
                return true;
            }
        }
        return false;
    }

    private boolean isWordInSentence(String sentence, String word) {
        sentence=sentence.toLowerCase();
        word=word.toLowerCase();
        if (sentence.contains(" " + word + " ")||(sentence.startsWith(word+" ")||sentence.endsWith(" "+word)||sentence.equals(word))) {
            return true;
        }
        return false;
    }
}
