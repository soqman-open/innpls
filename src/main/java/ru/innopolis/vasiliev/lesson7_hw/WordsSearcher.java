package ru.innopolis.vasiliev.lesson7_hw;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

class WordsSearcher {
    private final int THREADS_COUNT_IN_POOL=10;
    private final static int THRESHOLD =10_000;
    private final static ThresholdString thresholdString=new ThresholdString(THRESHOLD);
    private final static Logger infoParsingTiming=LogManager.getLogger("infoParsingTiming");
    private final static Logger logger=LogManager.getLogger(ScanningRunnable.class);
    private final static Logger errorsEX=LogManager.getLogger("errorsEX");


    public void getOccurrences(String[] sources, String[] words, String res) {
        if (sources.length == 0 || words.length == 0 || res.equals("")) {
           logger.error("Check arguments");
            return;
        }
        int i = 0;
        ThresholdStringSaver.setResultPath(res);
        long startTime=System.currentTimeMillis();
        infoParsingTiming.info("parsing started");
        ExecutorService executorService = Executors.newFixedThreadPool(THREADS_COUNT_IN_POOL);
        deletePreviousResult(res);
        WordsFounder wordsFounder=new WordsFounder(words);
        for (String source : sources) {
            executorService.submit(new ScanningRunnable("runnable-" + i++,source,wordsFounder,thresholdString ));
        }
        executorService.shutdown();
        try {
            executorService.awaitTermination(1,TimeUnit.HOURS);
            ThresholdStringSaver.save(thresholdString);
            infoParsingTiming.info("Elapsed time: "+(System.currentTimeMillis()-startTime));
        } catch (InterruptedException e) {
            errorsEX.error(e.getMessage());
        }
    }

    private void deletePreviousResult(String res) {
        if (Files.exists(Paths.get(res))) {
            try {
                Files.delete(Paths.get(res));
            } catch (IOException e) {
                errorsEX.error(e.getMessage());
            }
        }
    }
}



