package ru.innopolis.vasiliev.lesson7_hw;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.Scanner;

class ScanningRunnable implements Runnable {

    private final String src;
    private final String name;
    private ThresholdString thresholdString;
    private WordsFounder wordsFounder;

    private final static Logger logger=LogManager.getLogger(ScanningRunnable.class);
    private final static Logger errorsEX=LogManager.getLogger("errorsEX");

    ScanningRunnable(String name, String src, WordsFounder wordsFounder, ThresholdString thresholdString){
        this.name = name;
        this.src = src;
        this.wordsFounder = wordsFounder;
        this.thresholdString=thresholdString;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public void run() {
        logger.info("\""+name + "\" in \"" + Thread.currentThread().getName()+"\" started");
        scan();
    }

    private void scan(){
        String result;
        File file = new File(src);
        try (Scanner scanner = new Scanner(file)) {
            while (scanner.hasNext()) {
                scanner.useDelimiter("\\. ");
                result = scanner.next();
                if(wordsFounder.search(result)){
                    thresholdString.addString(dotAppend(result));
                    if(thresholdString.isFull()) ThresholdStringSaver.save(thresholdString);
                }
            }
        } catch (FileNotFoundException e) {
            errorsEX.error(e.getMessage());
        }
    }

    private static String dotAppend(String s){
        return s+". ";
    }
}