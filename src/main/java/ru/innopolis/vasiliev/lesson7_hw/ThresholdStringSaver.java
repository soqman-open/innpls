package ru.innopolis.vasiliev.lesson7_hw;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
class ThresholdStringSaver {

    private static String resultPath;

    private final static Logger logger=LogManager.getLogger(ThresholdStringSaver.class);
    private final static Logger errorsEX=LogManager.getLogger("errorsEX");

    public static String getResultPath() {
        return resultPath;
    }

    public static void setResultPath(String resultPath) {
        ThresholdStringSaver.resultPath = resultPath;
    }

    public static synchronized void save(ThresholdString thresholdString){
        logger.info("Save");
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(resultPath, true))) {
            bufferedWriter.write(thresholdString.getString());
        } catch (FileNotFoundException e) {
            errorsEX.error(e.getMessage());
        } catch (IOException o) {
            errorsEX.error(o.getMessage());
        }
    }
}
