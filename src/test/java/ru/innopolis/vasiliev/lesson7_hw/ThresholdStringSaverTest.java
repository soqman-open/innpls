package ru.innopolis.vasiliev.lesson7_hw;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class ThresholdStringSaverTest {
    private final static Logger logger=LogManager.getLogger(ResGenerator.class);
    private final static Logger errorsEX=LogManager.getLogger("errorsEX");
    private static final String path="out.txt";
    private static final String s="TEST";
    @BeforeClass
    public static void setResultPath(){
        ThresholdStringSaver.setResultPath(path);
    }
    @Test
    public void saveTest(){
        ThresholdString thresholdString=mock(ThresholdString.class);
        when(thresholdString.getString()).thenReturn(s);
        ThresholdStringSaver.save(thresholdString);
        assertTrue(read().equals(s));
    }

    private String read(){
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(path))) {
            return bufferedReader.readLine();
        } catch (FileNotFoundException e) {
            errorsEX.error(e.getMessage());
        } catch (IOException o) {
            errorsEX.error(o.getMessage());
        }
        return "";
    }

    @AfterClass
    public static void deleteTempFile(){
        try {
            Files.delete(Paths.get(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
