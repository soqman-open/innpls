package ru.innopolis.vasiliev.lesson7_hw;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class ScanningRunnableTest {
    private final static Logger errorsEX=LogManager.getLogger("errorsEX");
    private final static Logger logger=LogManager.getLogger(ResGenerator.class);
    private static ScanningRunnable scanningRunnable;
    private final static String name="name";
    private final static String path="t_src.txt";
    private final static String src="Aaaaaaa. bbbbb. Ccccc. D.";
    private static ThresholdString thresholdString;
    private static WordsFounder wordsFounder;

    @BeforeClass
    public static void initScanningRunnable(){
        createTempFile();
        thresholdString=mock(ThresholdString.class);
        when(thresholdString.isFull()).thenReturn(true);
        wordsFounder=mock(WordsFounder.class);
        when(wordsFounder.search(anyString())).thenReturn(true);
        scanningRunnable=new ScanningRunnable(name,path,wordsFounder,thresholdString);
    }

    @Test
    public void runTest(){
        Thread thread=new Thread(scanningRunnable);
        thread.start();
        //? не доделал. тут сложности
    }

    private static void createTempFile(){
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(path))) {
            bufferedWriter.write(src);
        } catch (FileNotFoundException e) {
            errorsEX.error(e.getMessage());
        } catch (IOException o) {
            errorsEX.error(o.getMessage());
        }
    }

    @AfterClass
    public static void deleteTempFile(){
        try {
            Files.delete(Paths.get(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
