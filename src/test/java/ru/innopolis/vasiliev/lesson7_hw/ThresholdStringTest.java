package ru.innopolis.vasiliev.lesson7_hw;

import org.junit.After;
import org.junit.Test;
import static org.junit.Assert.*;

public class ThresholdStringTest {
    private static final int threshold=10;
    private static final String s="12345";
    ThresholdString thresholdString=new ThresholdString(threshold);

    @After
    public void refresh(){
        thresholdString=new ThresholdString(threshold);
    }

    @Test
    public void addStringTest(){
        thresholdString.addString(s);
        assertTrue(!thresholdString.isFull() && thresholdString.getString().equals(s));
        thresholdString.addString(s);
        assertTrue(thresholdString.isFull() && thresholdString.getString().equals(s+s));
    }

    @Test
    public void setThresholdTest(){
        thresholdString.addString(s);   //зависимый тест!
        thresholdString.setThreshold(0);
        assertTrue(thresholdString.isFull());
        thresholdString.setThreshold(-2132323223);
        assertTrue(thresholdString.isFull()&&thresholdString.getThreshold()==0);
        thresholdString.setThreshold(6);
        assertTrue(!thresholdString.isFull());
    }
    @Test
    public void cleanTest(){
        thresholdString.addString(s+s+s);   //зависимый тест!
        thresholdString.clean();
        assertTrue(!thresholdString.isFull()&&thresholdString.getString().isEmpty());
    }


}
