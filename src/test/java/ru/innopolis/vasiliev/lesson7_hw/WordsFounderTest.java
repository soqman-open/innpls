package ru.innopolis.vasiliev.lesson7_hw;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
public class WordsFounderTest {
    private static WordsFounder wordsFounder;
    private static final String [] words={"Abc","zz","a"};
    private static final String s1="zzz zzz abc";
    private static final String s2="zzz zzz";

    @Before
    public void initWordsFounder(){
        wordsFounder=new WordsFounder(words);
    }

    @Test
    public void searchTestPositive(){
        assertTrue(wordsFounder.search(s1));
    }

    @Test
    public void searchTestNegative(){
        assertTrue(!wordsFounder.search(s2));
    }
}
