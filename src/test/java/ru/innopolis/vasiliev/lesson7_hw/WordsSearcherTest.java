package ru.innopolis.vasiliev.lesson7_hw;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import java.io.File;

//это мой старый класс для проверки. его пока не трогал

public class WordsSearcherTest {
    private String[] words = {"sd","gfg","fg","fgf","kyk","nm","fhfdfgh","dghj"};
    private final static Logger logger=LogManager.getLogger(ScanningRunnable.class);
    WordsSearcher wordsSearcher = new WordsSearcher();

    @Test
    public void getOccurrencesTest() {
        wordsSearcher.getOccurrences(getSourcesPaths(ResGenerator.OUTPUT_PATH), words, ResGenerator.OUTPUT_PATH + "out.txt");
    }

    public String[] getSourcesPaths(String path) {
        File dir = new File(path);
        File[] matches = dir.listFiles((dir1, name) -> name.startsWith(ResGenerator.SOURCES_FILEPREFIX) && name.endsWith(ResGenerator.SOURCES_FILEEXT));
        String[] sources = new String[matches.length];
        for (int i = 0; i < sources.length; i++) {
            sources[i] = matches[i].getAbsolutePath();
        }
        return sources;
    }
}
